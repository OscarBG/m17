﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    //Cuando el jugador colisiona con el powerup y se destruye.
        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.tag == "Jugador")
            {
            GameObject.Destroy(this.gameObject);

            }
        }
    
}
