﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{
    public int jumpForce;
    public int horizontalVelocity;
    private bool saltar;
    private bool mas;
    private int altura = -4;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space") && saltar == true)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
            saltar = false;
        }
        //le aplica la velocidad todo el rato
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalVelocity, this.GetComponent<Rigidbody2D>().velocity.y);

        //si la posicion del objeto en y es mas pequeña que la altura de la variable
        if (this.transform.position.y <= altura)
        {
            //recarga la escena
            SceneManager.LoadScene("SampleScene");
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "Obstacle")
        {
            //Mata al jugador cuando impacta con el objeto
            GameObject.Destroy(this.gameObject);
            //Esto reloadea la escena, para no tener que ir dandole al start todo el rato.
            SceneManager.LoadScene("SampleScene");
        }

        //esto es cuando colisiona con los rombos verdes le da un salto extra
        if (coll.tag == "PU")
        {
            saltar = true;
        }

        if (coll.tag == "Jumper")
        {
            //Cuando colisiona con el jumper le da una fuerza
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce + 900 * 2));
            saltar = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            //pone el booleano de saltar en true cuando impacta con el suelo
            saltar = true;
        }
        if (collision.gameObject.tag == "Obstacle")
        {
            //Mata al jugador cuando impacta con el objeto
            GameObject.Destroy(this.gameObject);
            //Esto reloadea la escena, para no tener que ir dandole al start todo el rato.
            SceneManager.LoadScene("SampleScene");
        }

    }


}