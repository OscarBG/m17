﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balls : MonoBehaviour
{
    public int f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //basicamente le da una fuerza a una bola para que se inmole contra el jugador.
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-f, 0));
    }
}
