﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingEnemy : MonoBehaviour
{
    public int jumpForcex;
    public int jumpForcey;
    private bool right;

    // Start is called before the first frame update
    void Start()
    {
        //primero le doy una fuerza para que se mueva de posición y le pongo el boleano en true.
        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(jumpForcex, jumpForcey));
        right = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Esto es el movimiento perpetuo de la bola, para que siempre este en movimiento entre dos puntos.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            if (right)
            {
                //cuando impacta con el suelo le quito toda la velocidad que lleve para que no de problemas
                this.GetComponent<Rigidbody2D>().velocity = (new Vector2(0, 0));
                //luego el pongo otra para que se mueva en dirección contraria a la del start y le cambio el booleano
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-jumpForcex, jumpForcey));
                right = false;
            }
            else
            {
                //cuando impacta con el suelo le quito toda la velocidad que lleve para que no de problemas
                this.GetComponent<Rigidbody2D>().velocity = (new Vector2(0, 0));
                //luego el pongo otra para que se mueva en la misma dirección del start y le cambio el booleano
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(jumpForcex, jumpForcey));
                right = true;
            }
        }
    }
}
