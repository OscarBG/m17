﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{

    public Player player;
    public Camera gameCamera;
    public Text Score;
    private int s = -10;

    //todos los prefabs disponibles
    public GameObject[] blockPrefabs;
    //puntero, apunta el final de los bloques generados
    private float puntero;
    //spawn, la variable extra que tiene que "mirar" el puntero para saber si generar un nuevo bloque
    private float spawn = 20;
    // Start is called before the first frame update
    void Start()
    {
        //justo al salir creamos un bloque inicial
        Instantiate(blockPrefabs[0]);
        //Hago que el score empiece en -10 porque tambíen cuenta cuando spawnea el init.
        s = -10;
    }

    // Update is called once per frame
    void Update()
    {
        //Cosas del score
        Score.text = "Score: "+ s;
        //Score.color = Color.cyan;
        Score.fontSize = 20;
        if (player != null)
        {
            //hacemos que la camera siga al jugador. La camara usa un vector3
            gameCamera.transform.position = new Vector3(
                player.transform.position.x,
                player.transform.position.y,
                gameCamera.transform.position.z);
        }
        if (player != null && this.puntero < player.transform.position.x + spawn )
        {
            //instanciar hace aparecer en la escena activa
            GameObject newBlock = Instantiate(blockPrefabs[Random.Range(1, blockPrefabs.Length)]);
            //score +10
            s += 10;
            //cogemos el tamaño, que seria la escala (tamaño) de x, del primer hijo (el suelo) del bloque prefab
            float size = newBlock.transform.GetChild(0).transform.localScale.x;
            //ponemos la posicion del tamaño instanciado en la posicion del puntero + la mitad de su tamaño (la posicion que tenemos que indicarle es su centro absoluto)
            newBlock.transform.position = new Vector2(puntero+size/2, 0);
            //aumentamos el puntero en el tamaño del bloque
            puntero += size;

        }
    }
}
