﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPlatCentral2 : MonoBehaviour
{
    //Movimiento en Y
    private void Awake()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 5f);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Tope")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 5f);
        }
        else if (collision.gameObject.name == "Tope1")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -5f);
        }
    }
}
