﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPlatCentral : MonoBehaviour
{
    //Movimiento en X
    private void Awake()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(5f, this.GetComponent<Rigidbody2D>().velocity.y);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "Tope")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-5f, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (collision.gameObject.name == "Tope1")
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(5f, this.GetComponent<Rigidbody2D>().velocity.y);
        }
    }
}
