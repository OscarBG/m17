﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cajaController : MonoBehaviour
{
    public Animator animator;
    public GameObject proyectil;

    public delegate void Curasiones(float vida);
    public static event Curasiones CurasEvent;

    private void Update()
    {
        //Por si se cae del mapa
        if(this.transform.position.y < -50)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si colisiona con cualquiera de los 2 players
        if (collision.gameObject.name == "HitboxInferior")
        {
            var r = UnityEngine.Random.Range(0, 2);
            //Tocan proyectiles
            if (r == 1)
            {
                this.animator.SetBool("Explosion", true);
                StartCoroutine(Balasos(3));
            }
            //Toca curacion
            else if (r == 0)
            {
                this.animator.SetBool("Buffo", true);
                StartCoroutine(Curas(3));
            }
        }
    }

    //Cura a Luffy 10hp
    IEnumerator Curas(float f)
    {
        yield return new WaitForSeconds(f);
        CurasEvent(10);
        Destroy(this.gameObject);
    }

    //Crea 4 proyectiles que van en diferentes direcciones 
    IEnumerator Balasos (float f)
    {
        yield return new WaitForSeconds(f);
        Destroy(this.gameObject);

        //proyectil 1
        GameObject newBlock = Instantiate(proyectil);
        newBlock.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y +2);
        newBlock.transform.Rotate(new Vector3(0f,0f,-45f), this.transform.position.x);
        newBlock.GetComponent<Rigidbody2D>().AddForce(new Vector3(200f, 200f));
        newBlock.gameObject.name = "proyectil";

        //proyectil 2
        GameObject newBlock2 = Instantiate(proyectil);
        newBlock2.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y +7);
        newBlock2.transform.Rotate(new Vector3(0f, 0f, -45f), this.transform.position.x);
        newBlock2.GetComponent<Rigidbody2D>().AddForce(new Vector3(200f, 200f));
        newBlock2.gameObject.name = "proyectil";

        //proyectil 3
        GameObject newBlock3 = Instantiate(proyectil);
        newBlock3.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y + 2);
        newBlock3.transform.Rotate(new Vector3(0f, 0f, 45f), this.transform.position.x);
        newBlock3.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200f, 200f));
        newBlock3.gameObject.name = "proyectil";

        //proyectil 4
        GameObject newBlock4 = Instantiate(proyectil);
        newBlock4.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y + 7);
        newBlock4.transform.Rotate(new Vector3(0f, 0f, 45f), this.transform.position.x);
        newBlock4.GetComponent<Rigidbody2D>().AddForce(new Vector2(-200f, 200f));
        newBlock4.gameObject.name = "proyectil";
    }
}
