﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class YugiController : MonoBehaviour
{
    public int speed = 10;
    public int fuerzaSalto = 0;
    public bool stuneado = false;
    public bool salto = true;
    public float hp = 100;

    public bool bloquear;
    public bool escudo = true;
    public float escudoHP = 50f;
    public bool escudoActivo;

    public GameObject cSlifer;
    public GameObject cObelisk;
    public GameObject cRa;
    private GameObject carta = null;
    public bool Slifer;
    public bool Obelisk;
    public bool Ra;
    public bool cartaActiva;
    public GameObject cartaSprite;
    public bool cdCartas;

    public int status;
    private bool pararCombo;
    private bool pegar;

    public Text LuffyWin;


    //evento
    public delegate void Damage(float vida);
    public event Damage DamageEvent;

    // Start is called before the first frame update
    void Start()
    {
        escudo = true;
        escudoActivo = true;

        Slifer = false;
        Obelisk = false;
        Ra = false;
        cartaActiva = false;
        cdCartas = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Cuando el personaje fenece
        if (hp <= 0)
        {
            this.GetComponent<Animator>().SetBool("Bloquear", false);
            this.GetComponent<Animator>().SetBool("Mov", false);
            this.GetComponent<Animator>().SetTrigger("Muerte");
            LuffyWin.gameObject.SetActive(true);
            Destroy(this);
        }
        else
        {
            //Cuando el personaje cae del escenario
            if (this.transform.position.y < -50)
            {
                hp = 0;
                DamageEvent(hp);
            }
            //Pegar y combos
            if (Input.GetKeyDown("[3]"))
            {
                if (salto)
                {
                    if (!bloquear)
                    {
                        if (status == 1)
                        {
                            pegar = true;
                            GameObject hitboxPegar = this.gameObject.transform.GetChild(2).gameObject;
                            hitboxPegar.SetActive(true);
                            this.GetComponent<Animator>().SetBool("Mov", false);
                            this.GetComponent<Animator>().SetTrigger("Ataque2");
                            StartCoroutine(PegarParadaNormal(1f, hitboxPegar));
                        }
                        else
                        {
                            pararCombo = true;
                            status = 0;
                        }
                        if (status == 0 && !pegar)
                        {
                            pegar = true;
                            GameObject hitboxPegar = this.gameObject.transform.GetChild(2).gameObject;
                            hitboxPegar.SetActive(true);
                            this.GetComponent<Animator>().SetBool("Mov", false);
                            this.GetComponent<Animator>().SetTrigger("Ataque1");
                            pararCombo = false;
                            StartCoroutine(MaxCombo(0.5f, status));
                            StartCoroutine(MinCombo(0.2f, 1));
                            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                            StartCoroutine(PegarParadaNormal(0.8f, hitboxPegar));
                        }
                    }
                }
            }

            if (stuneado == false && !pegar)
            {
                //Moverse a la derecha como españa
                if (Input.GetKey("right"))
                {
                    this.GetComponent<Animator>().SetBool("Mov", true);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(speed, this.GetComponent<Rigidbody2D>().velocity.y);
                    this.transform.localScale = new Vector2(1.5f, 1.5f);
                }
                //Moverse a la izquierda
                else if (Input.GetKey("left"))
                {
                    this.GetComponent<Animator>().SetBool("Mov", true);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
                    this.transform.localScale = new Vector2(-1.5f, 1.5f);
                }
                else
                {
                    this.GetComponent<Animator>().SetBool("Mov", false);
                    this.GetComponent<Rigidbody2D>().velocity = new Vector3(this.GetComponent<Rigidbody2D>().velocity.x, this.GetComponent<Rigidbody2D>().velocity.y);
                }
                //Saltar
                if (Input.GetKeyDown("up") && salto)
                {
                    this.GetComponent<Animator>().SetTrigger("Saltar");
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, fuerzaSalto));
                    this.GetComponent<Animator>().SetBool("Mov", false);
                    salto = false;
                }
                //Bloquear
                else if (Input.GetKey("down"))
                {
                    if (escudo == true && escudoHP > 0)
                    {
                        bloquear = true;
                        escudoActivo = true;
                        StartCoroutine(Escudico(1));
                        this.GetComponent<Animator>().SetBool("Bloquear", true);
                        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, this.GetComponent<Rigidbody2D>().velocity.y);
                    }
                }
                else
                {
                    bloquear = false;
                    escudoActivo = false;
                    this.GetComponent<Animator>().SetBool("Bloquear", false);
                }
            }
        }

        //Cartas de Yugi
        if (carta != null)
        {
            carta.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 4);
        }
        if (Input.GetKeyDown("[1]"))
        {
            SelecciondeCarta();
        }
        if (Input.GetKeyDown("[2]"))
        {
            LanzarCarta();
        }
    }

    //Funcion que selecciona la carta que va a lanzarse
    private void SelecciondeCarta()
    {
        Slifer = false;
        Obelisk = false;
        Ra = false;
        
        if (carta != null)
        {
            Destroy(carta.gameObject);
        }
        var r = Random.Range(0, 3);
        //Toca Obelisk
        if (r == 0)
        {
            carta = Instantiate(cObelisk);
            carta.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 4);
            Obelisk = true;
            cartaActiva = true;
        }
        //Toca Slifer
        else if (r == 1)
        {
            carta = Instantiate(cSlifer);
            carta.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 4);
            Slifer = true;
            cartaActiva = true;
        }
        //Toca Ra
        else if (r == 2)
        {
            carta = Instantiate(cRa);
            carta.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 4);
            Ra = true;
            cartaActiva = true;
        }
    }

    //Funcion de lanzar la carta previamente seleccionada
    private void LanzarCarta()
    {
        if (cartaActiva && cdCartas)
        {
            cdCartas = false;
            cartaActiva = false;
            StartCoroutine(cdCarta(2));
            this.GetComponent<Animator>().SetTrigger("Lanzar");
            //Slifer seleccionado
            if (Slifer)
            {
                if (this.transform.localScale.x == -1.5)
                {
                    Destroy(this.carta.gameObject);
                    GameObject carta = Instantiate(cartaSprite);
                    carta.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y);
                    carta.GetComponent<Rigidbody2D>().velocity = new Vector3(-10f, 0f);
                    carta.gameObject.name = "Slifer";
                    StartCoroutine(movcarta(-0.2f, 1, carta));
                    StartCoroutine(movcarta(0.4f, 2, carta));
                }
                else if (this.transform.localScale.x == 1.5)
                {
                    Destroy(this.carta.gameObject);
                    GameObject carta = Instantiate(cartaSprite);
                    carta.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y);
                    carta.GetComponent<Rigidbody2D>().velocity = new Vector3(10f, 0f);
                    carta.gameObject.name = "Slifer";
                    StartCoroutine(movcarta(-0.2f, 3, carta));
                    StartCoroutine(movcarta(0.4f, 4, carta));
                }

            }
            //Obelisk seleccionado
            if (Obelisk)
            {
                if (this.transform.localScale.x == -1.5)
                {
                    Destroy(this.carta.gameObject);
                    GameObject carta = Instantiate(cartaSprite);
                    carta.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y);
                    carta.GetComponent<Rigidbody2D>().velocity = new Vector3(-10f, 0f);
                    carta.gameObject.name = "Obelisk";
                    StartCoroutine(movcarta(-0.2f, 1, carta));
                    StartCoroutine(movcarta(0.4f, 2, carta));
                }
                else if (this.transform.localScale.x == 1.5)
                {
                    Destroy(this.carta.gameObject);
                    GameObject carta = Instantiate(cartaSprite);
                    carta.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y);
                    carta.GetComponent<Rigidbody2D>().velocity = new Vector3(10f, 0f);
                    carta.gameObject.name = "Obelisk";
                    StartCoroutine(movcarta(-0.2f, 3, carta));
                    StartCoroutine(movcarta(0.4f, 4, carta));
                }

            }
            //Ra seleccionado
            if (Ra)
            {
                if (this.transform.localScale.x == -1.5)
                {
                    Destroy(this.carta.gameObject);
                    GameObject carta = Instantiate(cartaSprite);
                    carta.transform.position = new Vector2(this.transform.position.x - 2, this.transform.position.y);
                    carta.GetComponent<Rigidbody2D>().velocity = new Vector3(-10f, 0f);
                    carta.gameObject.name = "Ra";
                    StartCoroutine(movcarta(-0.2f, 1, carta));
                    StartCoroutine(movcarta(0.4f, 2, carta));
                }
                else if (this.transform.localScale.x == 1.5)
                {
                    Destroy(this.carta.gameObject);
                    GameObject carta = Instantiate(cartaSprite);
                    carta.transform.position = new Vector2(this.transform.position.x + 2, this.transform.position.y);
                    carta.GetComponent<Rigidbody2D>().velocity = new Vector3(10f, 0f);
                    carta.gameObject.name = "Ra";
                    StartCoroutine(movcarta(-0.2f, 3, carta));
                    StartCoroutine(movcarta(0.4f, 4, carta));
                }
            }
        }
    }

    //CD de lanzar las cartas
    IEnumerator cdCarta(float f)
    {
        yield return new WaitForSeconds(f);
        cdCartas = true;
    }
    
    //Combos
    IEnumerator PegarParadaNormal(float v, GameObject ataque)
    {
        yield return new WaitForSeconds(v);
        if (pegar == true)
        {
            pegar = false;
        }
    }
    IEnumerator MinCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (!pararCombo)
        {
            status = st;
        }

    }
    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status != st)
        {
            status = 0;
            pararCombo = true;
        }
    }
    IEnumerator PegarParada(float f)
    {
        yield return new WaitForSeconds(f);
        if (pegar == true)
        {
            pegar = false;
        }
    }

    //(esto no esta acabado)
    IEnumerator Escudico(float f)
    {
        yield return new WaitForSeconds(f);

        if (escudoHP == 0)
        {
            escudo = false;
            StartCoroutine(RegenerarEscudico(10));
        }

        else if (escudoHP > 0)
        {
        }

    }
    //(esto no esta acabado)
    IEnumerator RegenerarEscudico(float f)
    {
        yield return new WaitForSeconds(f);
        escudo = true;
        escudoHP = 50;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("muroabajo"))
        {
            salto = true;
        }
        //Colision con los proyectiles de la caja
        if (collision.gameObject.name == "proyectil" && !bloquear)
        {
            Debug.Log("proyectil de caja ha pegado a yugi el fullas");
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-3050f, 100f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(3050f, 100f));
            }
            hp -= 5;
            if (hp <= 0)
            {
                Debug.Log("f Yugi");
                hp = 0;
            }
            DamageEvent(hp);
        }
        //Colision con la roca
        if (collision.gameObject.name == "roca" && !bloquear)
        {
            Debug.Log("roca ha pegado a yugi el fullas");
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-3050f, 100f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(3050f, 100f));
            }
            hp -= 5;
            if (hp <= 0)
            {
                Debug.Log("f Yugi");
                hp = 0;
            }
            DamageEvent(hp);
        }

        //Colision con la hitbox de daño de luffy
        if (collision.gameObject.name == "HitboxPegar" && !bloquear)
        {
            if (this.transform.localScale.x == 1.5f)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-3050f, 100f));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(3050f, 100f));
            }
            hp -= 5;
            if (hp <= 0)
            {
                Debug.Log("f Yugi");
                hp = 0;
            }
            DamageEvent(hp);
        }
    }

    //Stun de la carta roja
    IEnumerator cdSlifer(float f)
    {
        yield return new WaitForSeconds(f);
        stuneado = false;
    }

    //Cambio de movimiento de la carta en el aire
    IEnumerator movcarta(float f, float state, GameObject carta)
    {
        yield return new WaitForSeconds(f);

        if (state == 1)
        {
            if (carta != null)
                carta.GetComponent<Rigidbody2D>().AddForce(new Vector2(-600f, 600f));
        }
        if (state == 2)
        {
            if (carta != null)
                carta.GetComponent<Rigidbody2D>().AddForce(new Vector2(-300f, 1000f));
        }
        if (state == 3)
        {
            if (carta != null)
                carta.GetComponent<Rigidbody2D>().AddForce(new Vector2(600f, 600f));
        }
        if (state == 4)
        {
            if (carta != null)
                carta.GetComponent<Rigidbody2D>().AddForce(new Vector2(300f, 1000f));
        }

    }
}
